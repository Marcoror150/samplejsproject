//Marc Christensen
//9/17/15
//Project 1
//120-115
// Convert some lengths

alert( "Length Converter" );

var lengthIn = prompt( "Enter a length in inches: " );
var lengthCm = lengthIn * 2.54;

alert( lengthIn + " inches is equivalent to " + lengthCm + " centimeters." );